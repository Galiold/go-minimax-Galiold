# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Galiold/Documents/University/5th Sem - Fall 2018/Artificial Intelligence/Project/Game/go-minimax-Galiold/BaseGo/gtp/gtp.cpp" "/Users/Galiold/Documents/University/5th Sem - Fall 2018/Artificial Intelligence/Project/Game/go-minimax-Galiold/BaseGo/cmake-build-debug/gtp/CMakeFiles/GTP.dir/gtp.cpp.o"
  "/Users/Galiold/Documents/University/5th Sem - Fall 2018/Artificial Intelligence/Project/Game/go-minimax-Galiold/BaseGo/gtp/play_gtp.cpp" "/Users/Galiold/Documents/University/5th Sem - Fall 2018/Artificial Intelligence/Project/Game/go-minimax-Galiold/BaseGo/cmake-build-debug/gtp/CMakeFiles/GTP.dir/play_gtp.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "AppleClang")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../core"
  "../engine"
  "../gtp"
  "../sgf"
  "../utils"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
