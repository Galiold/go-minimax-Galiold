#include <iostream>
#include "board.h"
#include "core.h"

// use namespace std;

#define MAX_DEPTH 4

int bestMoveChosen;

/*
 * Use this function to reset your engine and prepare it for a new game
 */
void reset() {
    // TODO: Your code for resetting your engine goes here...
}


int colorCountDifference(int color){
    int otherColor = 0;
    if (color == 1)
        otherColor = 2;
    else if (color == 2)
        otherColor = 1;

    int colorCount = 0;
    int otherColorCount = 0;

    for (int i = 0; i < 9; ++i) {
        for (int j = 0; j < 9; ++j) {
            if (BOARD(i, j) == color)
                colorCount++;
            else if(BOARD(i, j ) == otherColor)
                otherColorCount++;
        }
    }


    return colorCount - otherColorCount;
}



int minimax(bool isMaxNode, int currentDepth, int row, int column, int color)
{
    int moveValue = 0;
    int captureRate = 0;
    int groupLiberty = 0;
    int colorDifference = 0;
    int otherColor = 0;


    if (color == 1)
        otherColor = 2;
    else if (color == 2)
        otherColor = 1;

    if (is_legal(POS(row, column),color)) {
        trymove(POS(row, column), color, nullptr, NO_MOVE);
        colorDifference = colorCountDifference(color);
        //groupLiberty = countlib(get_last_move()) * 3;
        moveValue = colorDifference + groupLiberty;
    }else{
      moveValue = -1;
    }

    // captureRate = does_capture_something(POS(row, column), color) * 10;


    if (currentDepth == MAX_DEPTH)
    {
        bestMoveChosen = POS(row, column);
        popgo();
        return moveValue;
    }


    if (isMaxNode)
    {
        isMaxNode = false;
        int tempMoveValue = 0;
        moveValue = INT32_MIN;

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {

                if (is_legal(POS(i, j), otherColor)) {
                    tempMoveValue = minimax(isMaxNode, currentDepth + 1, i, j, otherColor);
                    if (moveValue < tempMoveValue && moveValue != -1){
                        moveValue = tempMoveValue;

                        if (currentDepth == 0){
                          //popgo();
                          bestMoveChosen = POS(i, j);
                          //return moveValue;
                        }
                    }
                }

            }
        }
        popgo();

        return moveValue;
    }

    else {
        isMaxNode = true;
        int tempMoveValue = 0;
        moveValue = INT32_MAX;

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {

                if (is_legal(POS(i, j), otherColor)) {
                    tempMoveValue = minimax(isMaxNode, currentDepth + 1, i, j, otherColor);
                    if (moveValue > tempMoveValue && moveValue != -1){
                        moveValue = tempMoveValue;
                    }
                }
            }
        }
        popgo();
        return moveValue;
    }

}






/*
 * Should return a move for the input color
 *
 * If your move is to, say, place a stone on position (i, j) of the board, return POS(i, j)
 * Otherwise you may return PASS_MOVE
 */
int play(int color) {

    int test = minimax(true, 0, 0, 0, color);

    return bestMoveChosen;
}
