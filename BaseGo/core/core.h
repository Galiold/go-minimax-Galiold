//#include <fstream>
#ifndef _CORE_H_
#define _CORE_H_
//using namespace std;

//class logFile;
void reset();
int colorCountDifference(int color);
int minimax(bool isMaxNode, int currentDepth, int tryMove, int color);
int play(int color);



#endif  /* _CORE_H_ */
